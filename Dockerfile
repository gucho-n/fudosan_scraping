# ベースイメージの指定
FROM python:3.9

# 必要なパッケージのインストール
RUN pip install beautifulsoup4 jupyterlab requests selenium

# Jupyter Labの設定
RUN jupyter lab --generate-config
RUN echo "c.NotebookApp.token = ''" >> ~/.jupyter/jupyter_notebook_config.py

# ディレクトリの作成
WORKDIR /fudosan

# ポートの公開
EXPOSE 8888

# Jupyter Labの起動
CMD ["jupyter", "lab", "--ip=0.0.0.0", "--port=8888", "--allow-root"]
